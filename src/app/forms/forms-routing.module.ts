import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ValidationFormsComponent } from "./validation/validation-forms.component";
import { BasicComponent } from './layouts/basic/basic.component';
import { HorizontalComponent } from './layouts/horizontal/horizontal.component';
import { HiddenLabelsComponent } from './layouts/hidden-labels/hidden-labels.component';
import { InputsComponent } from './elements/inputs/inputs.component';
import { InputGroupsComponent } from './elements/input-groups/input-groups.component';
import { InputGridComponent } from './elements/input-grid/input-grid.component';
import { NGXFormWizardComponent } from './ngx-wizard/ngx-wizard.component';


import { JobInfoComponent } from './layouts/job-info/job-info.component';
import { CountryComponent } from './layouts/country/country.component';
import { InvoiceComponent } from './layouts/invoie-run/invoie-run.component';
import { PriceInfoComponent } from './layouts/price-info/price-info.component';
import { UserComponent } from './layouts/user/user.component';
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { AuditComponent } from './layouts/audit/audit.component';
import { ManualInvoiceComponent } from './layouts/manual-invoice/manual-invoice.component';
import { CreditNumberComponent } from './layouts/credit-number/credit-number.component';
import { ExtraChargesrComponent } from './layouts/extra-charges/extra-charges.component';
import { SysSecurityComponent } from './layouts/sys-security/sys-security.component';
import { ServiceCodeComponent } from './layouts/service-code/service-code.component';
import { PodSearchComponent } from './layouts/pod-search/pod-search.component';
import { InvoiceOptionComponent } from './layouts/invoice-option/invoice-option.component';
import { TariffCardComponent } from './layouts/new-tariff/new-tariff.component';
const routes: Routes = [
  {
    path: '',    
    children: [
	
     {
        path: 'job-info',
        component: JobInfoComponent,
        data: {
          title: 'Job Info'
        }
      },	
     {
        path: 'country',
        component: CountryComponent,
        data: {
          title: 'country Info'
        }
      },	
     {
        path: 'invoie-run',
        component: InvoiceComponent,
        data: {
          title: 'invoie-run Info'
        }
      },		
     {
        path: 'price-info',
        component: PriceInfoComponent,
        data: {
          title: 'price-info Info'
        }
      },	
     {
        path: 'user',
        component: UserComponent,
        data: {
          title: 'user Info'
        }
      },	
     {
        path: 'customer-info',
        component: CustomerInfoComponent,
        data: {
          title: 'customer-info Info'
        }
      },	
      {
        path: 'audit',
        component: AuditComponent,
        data: {
          title: 'audit Info'
        }
      },			
      {
        path: 'manual-invoice',
        component: ManualInvoiceComponent,
        data: {
          title: 'manual-invoice Info'
        }
      },		
      {
        path: 'credit-number',
        component: CreditNumberComponent,
        data: {
          title: 'credit-number Info'
        }
      },	
      {
        path: 'extra-charges',
        component: ExtraChargesrComponent,
        data: {
          title: 'extra-charges Info'
        }
      },		
      {
        path: 'sys-security',
        component: SysSecurityComponent,
        data: {
          title: 'sys-security Info'
        }
      },		
      {
        path: 'service-code',
        component: ServiceCodeComponent,
        data: {
          title: 'service-code Info'
        }
      },		
      {
        path: 'pod-search',
        component: PodSearchComponent,
        data: {
          title: 'pod-search Info'
        }
      },		
      {
        path: 'invoice-option',
        component: InvoiceOptionComponent,
        data: {
          title: 'invoice-option Info'
        }
      },		     
      {
        path: 'new-tariff',
        component: TariffCardComponent,
        data: {
          title: 'new-tariff Info'
        }
      },      
	
	
	
      {
        path: 'basic',
        component: BasicComponent,
        data: {
          title: 'Basic Forms'
        }
      },
      {
        path: 'horizontal',
        component: HorizontalComponent,
        data: {
          title: 'Horizontal Forms'
        }
      },
      {
        path: 'hidden-labels',
        component: HiddenLabelsComponent,
        data: {
          title: 'Hidden Labels'
        }
      },
      {
        path: 'inputs',
        component: InputsComponent,
        data: {
          title: 'Inputs'
        }
      },
      {
        path: 'input-groups',
        component: InputGroupsComponent,
        data: {
          title: 'Input Groups'
        }
      },
      {
        path: 'input-grid',
        component: InputGridComponent,
        data: {
          title: 'Input Grid'
        }
      },
      {
        path: 'validation',
        component: ValidationFormsComponent,
        data: {
          title: 'Validation Forms'
        }
      }, 
      {
        path: 'ngx',
        component: NGXFormWizardComponent,
        data: {
          title: 'NGX Wizard'
        }
      },     
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FormsRoutingModule { }
