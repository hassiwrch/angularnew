import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { FormsRoutingModule } from "./forms-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CustomFormsModule } from 'ng2-validation';
import { ArchwizardModule } from 'ng2-archwizard';
import { MatchHeightModule } from "../shared/directives/match-height.directive";

import { ValidationFormsComponent } from "./validation/validation-forms.component";
import { BasicComponent } from './layouts/basic/basic.component';

import { HorizontalComponent } from './layouts/horizontal/horizontal.component';
import { HiddenLabelsComponent } from './layouts/hidden-labels/hidden-labels.component';
import { InputsComponent } from './elements/inputs/inputs.component';
import { InputGroupsComponent } from './elements/input-groups/input-groups.component';
import { InputGridComponent } from './elements/input-grid/input-grid.component';
import { NGXFormWizardComponent } from './ngx-wizard/ngx-wizard.component';


import { JobInfoComponent } from './layouts/job-info/job-info.component';
import { CountryComponent } from './layouts/country/country.component';
import { InvoiceComponent } from './layouts/invoie-run/invoie-run.component';
import { PriceInfoComponent } from './layouts/price-info/price-info.component';
import { UserComponent } from './layouts/user/user.component'; 
import { CustomerInfoComponent } from './customer-info/customer-info.component';
import { AuditComponent } from './layouts/audit/audit.component';
import { ManualInvoiceComponent } from './layouts/manual-invoice/manual-invoice.component';
import { CreditNumberComponent } from './layouts/credit-number/credit-number.component';
import { ExtraChargesrComponent } from './layouts/extra-charges/extra-charges.component';
import { SysSecurityComponent } from './layouts/sys-security/sys-security.component';
import { ServiceCodeComponent } from './layouts/service-code/service-code.component';
import { PodSearchComponent } from './layouts/pod-search/pod-search.component';
import { InvoiceOptionComponent } from './layouts/invoice-option/invoice-option.component';
import { TariffCardComponent } from './layouts/new-tariff/new-tariff.component';
@NgModule({
    imports: [
        CommonModule,
        FormsRoutingModule,
        ReactiveFormsModule,
        FormsModule,
        CustomFormsModule,
        MatchHeightModule,
        ArchwizardModule,
        NgbModule
    ],
    declarations: [
        ValidationFormsComponent,
        BasicComponent,
		
		JobInfoComponent,
		CountryComponent,
		InvoiceComponent,
		PriceInfoComponent,
		UserComponent,
		CustomerInfoComponent,
		AuditComponent,
        ManualInvoiceComponent,
        CreditNumberComponent,
        ExtraChargesrComponent,
        SysSecurityComponent,
        ServiceCodeComponent,
        PodSearchComponent,
        InvoiceOptionComponent,
        TariffCardComponent,

        HorizontalComponent,
        HiddenLabelsComponent,
        InputsComponent,
        InputGroupsComponent,
        InputGridComponent,
        NGXFormWizardComponent
    ]

})
export class FormModule { }
