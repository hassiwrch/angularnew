import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

declare var require: any;
const data: any = require('../../shared/data/service-code.json');

@Component({
    selector: 'app-dt-service-code',
    templateUrl: './service-code.component.html',
    styleUrls: ['./service-code.component.scss']
})

export class ServiceCodeComponent {
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
		{ name: 'Code' },
		{ name: 'Description' },
		{ name: 'Number' },
		{ prop: 'st',name: '07:30' },
		{ prop: 'n0',name: '09:00' },
		{ prop: 'tt',name: '10:30' },
		{ prop: 'tw',name: '12:00' },
		{ prop: 'stt',name: '17:30' },
		{ prop: 'surcharge1',name: 'Surcharge' },
		{ prop: 'surcharge2',name: 'Surcharge' },
		{ prop: 'surcharge3',name: 'Surcharge' },
		{ prop: 'drate',name: 'D Rate' },
		
		{ name: 'LCR' }
    ];
  

    constructor() {
     
        this.rows = data;
    }


}