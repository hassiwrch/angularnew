import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

declare var require: any;
const data: any = require('../../shared/data/customer-file.json');

@Component({
    selector: 'app-dt-customer-file',
    templateUrl: './customer-file.component.html',
    styleUrls: ['./customer-file.component.scss']
})

export class CustomerFileComponent {
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
		{ name: 'Code' },
        { prop: 'name' },
        { name: 'Town' },
        { name: 'Country' },
        { name: 'Phone' }
    ];


    constructor() {
        this.rows = data;
    }


}