import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

declare var require: any;
const data: any = require('../../shared/data/customer-tariff -af.json');

@Component({
    selector: 'app-dt-customer-tariff-af',
    templateUrl: './customer-tariff-af.component.html',
    styleUrls: ['./customer-tariff-af.component.scss']
})

export class CustomerTariffAFComponent {
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
		{ name: 'Service' },
		{ name: 'Zone' },
		{ name: 'Description' },
		{ name: 'Currency' },
        { prop: 'active_date',name: 'Active Date' },
        { name: 'Product' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor() {
        this.temp = [...data];
        this.rows = data;
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.name.indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }
}