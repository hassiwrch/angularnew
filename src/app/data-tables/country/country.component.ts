import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

declare var require: any;
const data: any = require('../../shared/data/country.json');

@Component({
    selector: 'app-dt-country',
    templateUrl: './country.component.html',
    styleUrls: ['./country.component.scss']
})

export class CountryComponent {
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
		{ name: 'Country' },
		{ name: 'Code' },
		{ name: 'Default' },
		{ name: 'Z1' },
        { name: 'Z2' },
        { name: 'Z3' },
        { name: 'Z4' },
        { name: 'Z5' },
        { name: 'Z6' },
        { name: 'Z7' },
        { name: 'Z8' },
        { name: 'Z9' },
        { name: 'Z10' },
        { name: 'Agent' },
        { name: 'Zone' }
    ];


    constructor() {

        this.rows = data;
    }


}