import { Component } from '@angular/core';

//Declarations
declare var require: any;
const data: any = require('../../shared/data/employee.json');

@Component({
  selector: 'app-ready',
  templateUrl: './ready.component.html',
  styleUrls: ['./ready.component.scss']
})

export class ReadyComponent {
  rows = [];

  constructor() {
    this.rows = data;
  }
}