import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

declare var require: any;
const data: any = require('../../shared/data/customer-tariff.json');

@Component({
    selector: 'app-dt-customer-tariff',
    templateUrl: './customer-tariff.component.html',
    styleUrls: ['./customer-tariff.component.scss']
})

export class CustomerTariffComponent {
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
		{ name: 'Code' },
        { prop: 'tariff_card_name',name: 'Tariff Card Name' },
        { prop: 'last_action',name: 'Last Action' }
    ];
   

    constructor() {
     
        this.rows = data;
    }


}