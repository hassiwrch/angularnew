import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

declare var require: any;
const data: any = require('../../shared/data/shipment.json');

@Component({
    selector: 'app-dt-shipment',
    templateUrl: './shipment.component.html',
    styleUrls: ['./shipment.component.scss']
})

export class ShipmentComponent {
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
		{ name: 'Waybill' },
		{ name: 'Date' },
		{ name: 'Account' },
		{ name: 'City' },
        { name: 'Country' },
        { name: 'Weight' },
        { name: 'Pieces' },
        { name: 'Prod' },
        { name: 'Special' },
        { name: 'Upgrade' },
		{ name: 'Agent' },
        { name: 'Constigener' },
        
        { name: 'Manifest' },
        { name: 'Hawb' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor() {
        this.temp = [...data];
        this.rows = data;
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.name.indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }
}