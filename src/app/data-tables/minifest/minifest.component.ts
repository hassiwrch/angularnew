import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

declare var require: any;
const data: any = require('../../shared/data/shipment.json');

@Component({
    selector: 'app-dt-minifest',
    templateUrl: './minifest.component.html',
    styleUrls: ['./minifest.component.scss']
})

export class MinifestComponent {
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
		{ name: 'Waybill' },
		{ name: 'Date' },
		{ name: 'Account' },
		{ name: 'City' },
        { name: 'Country' },
        { name: 'Weight' },
        { name: 'Pieces' },
        { name: 'Prod' },
        { name: 'Special' },
        { name: 'Upgrade' },
		{ name: 'Agent' },
        { name: 'Constigener' },
        
        { name: 'Manifest' },
        { name: 'Hawb' }
    ];


    constructor() {

        this.rows = data;
    }


}