import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

declare var require: any;
const data: any = require('../../shared/data/credit-file.json');

@Component({
    selector: 'app-credit-note',
    templateUrl: './credit-note.component.html',
    styleUrls: ['./credit-note.component.scss']
})

export class CreditFileComponent {
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
		{ name: 'Invoice' },
        { name: 'Date' },
        { name: 'Account' },
        { name: 'Heading' }
    ];
  

    constructor() {
       
        this.rows = data;
    }


}