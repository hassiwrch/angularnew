import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DTFullScreenComponent } from "./fullscreen/dt-fullscreen.component";
import { DTEditingComponent } from "./editing/dt-editing.component";
import { DTFilterComponent } from "./filter/dt-filter.component";






import { CustomerFileComponent } from "./customer-file/customer-file.component";
import { CustomerTariffComponent } from "./customer-tariff/customer-tariff.component";
import { CustomerTariffAFComponent } from "./customer-tariff-af/customer-tariff-af.component";
import { CountryComponent } from "./country/country.component";
import { ShipmentComponent } from "./shipment/shipment.component";
import { ServiceCodeComponent } from "./service-code/service-code.component";
import { UserListComponent } from "./user-list/user-list.component";
import { CreditFileComponent } from "./credit-note/credit-note.component";
import { MinifestComponent } from "./minifest/minifest.component";
import { InvoicehistoryComponent } from "./invoice-history/invoice-history.component";
import { ManualComponent } from "./manual/manual.component";
import { DuplicateComponent } from "./duplicate/duplicate.component";
import { InvoiceFileComponent } from "./invoice-file/invoice-file.component";
import { ReadyComponent } from "./ready/ready.component";

import { DTPagingComponent } from "./paging/dt-paging.component";
import { DTPinningComponent } from "./pinning/dt-pinning.component";
import { DTSelectionComponent } from "./selection/dt-selection.component";
import { DTSortingComponent } from "./sorting/dt-sorting.component";
import { DTBasicComponent } from "./basic/dt-basic.component";
import { DTBasicComponent1 } from "./basic1/dt-basic.component1";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'fullscreen',
        component: DTFullScreenComponent,
        data: {
          title: 'Full Screen Data Table'
        }
      },
      {
        path: 'editing',
        component: DTEditingComponent,
        data: {
          title: 'Editing Data Table'
        }
      },
      {
        path: 'filter',
        component: DTFilterComponent,
        data: {
          title: 'Filter Data Table'
        }
      },






	  
	  {
        path: 'customer-file',
        component: CustomerFileComponent,
        data: {
          title: 'Filter Data Table'
        }
      },
	  {
        path: 'customer-tariff',
        component: CustomerTariffComponent,
        data: {
          title: 'Filter Data Table'
        }
      },
	  {
        path: 'customer-tariff-af',
        component: CustomerTariffAFComponent,
        data: {
          title: 'Filter Data Table'
        }
      },
	  {
        path: 'country',
        component: CountryComponent,
        data: {
          title: 'Filter Data Table'
        }
      },
	  {
        path: 'shipment',
        component: ShipmentComponent,
        data: {
          title: 'Filter Data Table'
        }
      },
	  {
        path: 'service-code',
        component: ServiceCodeComponent,
        data: {
          title: 'Filter Data Table'
        }
      },	
	  {
        path: 'user-list',
        component: UserListComponent,
        data: {
          title: 'Filter Data Table'
        }
      },
      {
        path: 'credit-note',
        component: CreditFileComponent,
        data: {
          title: 'credit-note Data Table'
        }
      },
      {
        path: 'minifest',
        component: MinifestComponent,
        data: {
          title: 'minifest Data Table'
        }
      },
      {
        path: 'invoice-history',
        component: InvoicehistoryComponent,
        data: {
          title: 'invoice-history Data Table'
        }
      },
      {
        path: 'manual',
        component: ManualComponent,
        data: {
          title: 'manual Data Table'
        }
      },
      {
        path: 'duplicate',
        component: DuplicateComponent,
        data: {
          title: 'duplicate Data Table'
        }
      },
      {
        path: 'invoice-file',
        component: InvoiceFileComponent,
        data: {
          title: 'invoice-file Data Table'
        }
      },
      {
        path: 'ready-file',
        component: ReadyComponent,
        data: {
          title: 'ready-file Data Table'
        }
      },









	
      {
        path: 'paging',
        component: DTPagingComponent,
        data: {
          title: 'Paging Data Table'
        }
      },
      {
        path: 'pinning',
        component: DTPinningComponent,
        data: {
          title: 'Pinning Data Table'
        }
      },
      {
        path: 'selection',
        component: DTSelectionComponent,
        data: {
          title: 'Selection Data Table'
        }
      },
      {
        path: 'sorting',
        component: DTSortingComponent,
        data: {
          title: 'Sorting Data Table'
        }
      },
      {
        path: 'basic',
        component: DTBasicComponent,
        data: {
          title: 'Basic Data Table'
        }
      },
      {
        path: 'basic1',
        component: DTBasicComponent1,
        data: {
          title: 'Basic Data Table'
        }
      },	  
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DataTablesRoutingModule { }