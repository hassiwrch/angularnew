import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { DataTablesRoutingModule } from "./data-tables-routing.module";

import { DTFullScreenComponent } from "./fullscreen/dt-fullscreen.component";
import { DTEditingComponent } from "./editing/dt-editing.component";
import { DTFilterComponent } from "./filter/dt-filter.component";






import { CustomerFileComponent } from "./customer-file/customer-file.component";
import { CustomerTariffComponent } from "./customer-tariff/customer-tariff.component";
import { CustomerTariffAFComponent } from "./customer-tariff-af/customer-tariff-af.component";
import { CountryComponent } from "./country/country.component";
import { ShipmentComponent } from "./shipment/shipment.component";
import { ServiceCodeComponent } from "./service-code/service-code.component";
import { UserListComponent } from "./user-list/user-list.component";
import { CreditFileComponent } from "./credit-note/credit-note.component";
import { MinifestComponent } from "./minifest/minifest.component";
import { InvoicehistoryComponent } from "./invoice-history/invoice-history.component";
import { ManualComponent } from "./manual/manual.component";
import { DuplicateComponent } from "./duplicate/duplicate.component";
import { InvoiceFileComponent } from "./invoice-file/invoice-file.component";
import { ReadyComponent } from "./ready/ready.component";


import { DTPagingComponent } from "./paging/dt-paging.component";
import { DTPinningComponent } from "./pinning/dt-pinning.component";
import { DTSelectionComponent } from "./selection/dt-selection.component";
import { DTSortingComponent } from "./sorting/dt-sorting.component";
import { DTBasicComponent } from "./basic/dt-basic.component";
import { DTBasicComponent1 } from "./basic1/dt-basic.component1";

@NgModule({
    imports: [
        CommonModule,
        DataTablesRoutingModule,
        NgxDatatableModule
    ],
    declarations: [
        DTFullScreenComponent,
        DTEditingComponent,
		
		
		CustomerFileComponent,
		CustomerTariffComponent,
		CustomerTariffAFComponent,
		CountryComponent,
		ShipmentComponent,
		ServiceCodeComponent,
        UserListComponent,
        CreditFileComponent,
        MinifestComponent,
        InvoicehistoryComponent,
        ManualComponent,
        DuplicateComponent,
        InvoiceFileComponent,
		ReadyComponent,
		
		
        DTFilterComponent,
        DTPagingComponent,
        DTPinningComponent,
        DTSelectionComponent,
        DTSortingComponent,
        DTBasicComponent,
        DTBasicComponent1,
    ]
})
export class DataTablesModule { }
