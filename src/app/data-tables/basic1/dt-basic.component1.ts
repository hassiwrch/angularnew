import { Component } from '@angular/core';

declare var require: any;
const data: any = require('../../shared/data/company.json');

@Component({
    selector: 'app-dt-basic',
    templateUrl: './dt-basic.component1.html',
    styleUrls: ['./dt-basic.component1.scss']
})

export class DTBasicComponent1 {
    rows = [];
    loadingIndicator: boolean = true;
    reorderable: boolean = true;

    // DataTable Content Titles
    columns = [
        { prop: 'name' },
        { name: 'Gender' },
        { name: 'Company' }
    ];

    constructor() {
        this.rows = data;
        setTimeout(() => { this.loadingIndicator = false; }, 1500);
    }
}