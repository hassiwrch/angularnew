import { Component, ViewChild } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

declare var require: any;
const data: any = require('../../shared/data/invoice.json');

@Component({
    selector: 'app-dt-invoice-history',
    templateUrl: './invoice-history.component.html',
    styleUrls: ['./invoice-history.component.scss']
})

export class InvoicehistoryComponent {
    rows = [];

    temp = [];

    // Table Column Titles
    columns = [
        { name: 'Account' },
        { name: 'Name' },
		{ name: 'Date' },
        { name: 'Vatable' },
        { prop: 'non_vatable',name: 'Non Vatable' },
        { prop: 'sub_tot',name: 'Sub Total' },
		{ name: 'Surcharge' },
        { name: 'Net' },
        { name: 'Discount' },
        { name: 'Vat' },
        { name: 'Total' },
        { name: 'Curr' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

    constructor() {
        this.temp = [...data];
        this.rows = data;
    }

    updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.name.indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }
}