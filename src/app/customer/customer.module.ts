import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";

import { CustomerRoutingModule } from "./customer-routing.module";
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { MatchHeightModule } from "../shared/directives/match-height.directive";
import { CustomerComponent } from "./customer.component";

@NgModule({
    imports: [
        CommonModule,
        CustomerRoutingModule,
        NgxChartsModule,
        NgbModule,
        MatchHeightModule
    ],
    declarations: [
        CustomerComponent,
   ]
})
export class CustomerModule { }
